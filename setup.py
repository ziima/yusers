"""Custom setup script."""

from distutils.command.build import build

from setuptools import setup


class custom_build(build):
    """Custom build command."""

    sub_commands = [('compile_catalog', None)] + build.sub_commands


def main():
    """Run setup."""
    setup(cmdclass={'build': custom_build})


if __name__ == '__main__':
    main()
