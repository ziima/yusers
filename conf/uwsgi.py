"""Set up yusers uWSGI application."""
from yusers import create_app

application = create_app()
