"""Yusers application settings."""
# ===== Flask settings =====
# Unique secret of the application
SECRET_KEY = '__SECRET_KEY__'

# ===== Yusers settings =====
# A admin username and password to LDAP.
YUSERS_LDAP_ADMIN_DN = '__ADMIN_DN__'
YUSERS_LDAP_ADMIN_PASSWORD = '__ADMIN_PASSWORD__'

# ===== Yusers-Mail settings =====
# Default sender
MAIL_DEFAULT_SENDER = '__DEFAULT_SENDER__'

# ===== Yusers-Babel settings =====
# Language of the application
BABEL_DEFAULT_LOCALE = '__LANGUAGE__'

# ===== Yusers-logging =====
YUSERS_LOGGING = {
    'version': 1,
    'formatters': {
        'simple': {'format': '%(asctime)s - %(name)s - %(levelname)-8s - %(message)s'},
    },
    'handlers': {
        'stdout': {
            'class': 'logging.StreamHandler',
            'formatter': 'simple',
        },
    },
    'loggers': {
        '': {
            'handlers': ['stdout'],
            'level': 'INFO',
        }
    }
}
