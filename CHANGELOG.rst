Changelog
=========

1.0.0~ynh2 (2024-11-29)
-----------------------

* Fixup upgrade script.
* Fixup README.

1.0.0~ynh1 (2024-11-29)
-----------------------

* Update yunohost integration to v1.0.0.
* Fix bumpversion.

1.0.0 (2024-11-29)
------------------

* Add python 3.12, 3.13.
* Drop python 3.7, 3.8.
* Switch to flask 3.0.
* Handle LDAP errors.
* Fix form design.
* Add admin DN to YunoHost config.
* Upgrade to Yunohost v2 app.
* Update project setup.
* Fix static checks.

0.5.3~ynh1 (2023-02-24)
-----------------------

* Revert changes from ``0.5.1~ynh1`` and ``0.5.2~ynh1``.
* Improve readme and add a test script.

0.5.2~ynh1 (2023-02-24)
-----------------------

* Fix for missing password in LDAP data (part II).

0.5.1~ynh1 (2023-02-24)
-----------------------

* Fix for missing password in LDAP data.

0.5.0~ynh1 (2023-02-24)
-----------------------

* Fix admin DN (upgrade to Yunohost 11.1).
* Fixup README.
* Fix project setup.

0.4.0~ynh1 (2021-05-21)
-----------------------

* Drop python 3.6, add python 3.10.
* Use single version for python and yunohost packages.
* Fix for WTForms 3+ and Flask 2.2.
* Fix yunohost install/upgrade.
* Upgrade venv packages.
* Avoid download in yunohost install.
* Add logging settings for yunohost.
* Fix removal of old files.
* Use new yunohost permissions.
* Update static checks and CI.

0.3.0 (2021-05-21)
------------------

* Add logging.
* Fix wording.
* Update CI.

0.2.0 (2021-01-26)
------------------

* Use template for reset email subject.
* Add support for custom templates.
* Refactor form templates.
* Add missing username to reset email.
* Fix name of reset email template.
* Compile catalog in install.
* Improve docs.
* Fix typo in Czech translation.
* Fix script name in yunohost nginx.
* Add socket directory to tmpfiles.

0.1.0 (2021-01-15)
------------------

* Index redirects to password reset.
* Add Czech translations.
* Add Yunohost integration.

0.0.1 (2021-01-05)
------------------

* Initial version.
