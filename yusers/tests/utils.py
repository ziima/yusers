from unittest import TestCase

from flask import Flask

from yusers import create_app


class FlaskTestCase(TestCase):
    app: Flask

    @classmethod
    def setUpClass(cls):
        cls.app = create_app(
            TESTING=True,
            SECRET_KEY='Gazpacho!',  # nosec
            YUSERS_LDAP_ADMIN_PASSWORD='Quagaars!',
            # Disable CSRF in tests.
            WTF_CSRF_ENABLED=False,
            MAIL_DEFAULT_SENDER='sender@example.org',
        )

    def setUp(self):
        self.client = self.app.test_client()
