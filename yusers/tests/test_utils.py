import os

from yusers.utils import render_template

from .utils import FlaskTestCase


class RenderTemplateTest(FlaskTestCase):
    def test_render_template(self):
        self.app.config['YUSERS_CUSTOM_TEMPLATES_DIR'] = os.path.join(os.path.dirname(__file__), 'templates')
        with self.app.test_request_context():
            output = render_template('base.html', var='TOTAL')

        self.assertIn('BASE TOTAL OVERRIDE', output)
