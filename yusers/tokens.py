"""Password reset token generator.

Based on django.contrib.auth.tokens.PasswordResetTokenGenerator,
see https://github.com/django/django/blob/master/django/contrib/auth/tokens.py
"""
import hashlib
import hmac
from datetime import date
from typing import Any

from flask import current_app


def _to_bytes(value: Any) -> bytes:
    if isinstance(value, bytes):
        return value
    else:
        return str(value).encode('utf-8')


class PasswordResetTokenGenerator:
    """Generator for password reset tokens."""
    salt = 'yusers.tokens.PasswordResetTokenGenerator'

    def make_token(self, uid_number: int, password: str) -> str:
        """Return a token to reset user password."""
        num_days = self.num_days(date.today())
        return self._make_token(uid_number, password, num_days)

    def check_token(self, uid_number: int, password: str, token: str) -> bool:
        """Check if password reset token is valid."""
        try:
            num_days_str, _ = token.split('-', 1)
        except ValueError:
            return False

        try:
            num_days = int(num_days_str)
        except ValueError:
            return False

        if not hmac.compare_digest(self._make_token(uid_number, password, num_days), token):
            return False

        if (self.num_days(date.today()) - num_days) > current_app.config['YUSERS_PASSWORD_RESET_TIMEOUT']:
            return False

        return True

    def _make_token(self, uid_number: int, password: str, num_days: int) -> str:
        key = hashlib.sha1(_to_bytes(self.salt + current_app.config['SECRET_KEY'])).digest()  # noqa: S324
        token = hmac.new(key, _to_bytes(uid_number) + _to_bytes(password) + _to_bytes(num_days), digestmod=hashlib.sha1)
        return str(num_days) + '-' + token.hexdigest()

    def num_days(self, today: date) -> int:
        """Return number of days since 2019-01-01."""
        return (today - date(2019, 1, 1)).days
