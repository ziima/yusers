"""LDAP utilities."""
from typing import cast

from flask import current_app
from ldap import SCOPE_SUBTREE  # type: ignore[import-untyped]
from ldap.ldapobject import LDAPObject, ReconnectLDAPObject  # type: ignore[import-untyped]

USERS_BASE = 'ou=users,dc=yunohost,dc=org'


def get_ldap_object() -> LDAPObject:
    """Return LDAP object."""
    ldap_obj = ReconnectLDAPObject(current_app.config['YUSERS_LDAP_URI'], retry_max=10, retry_delay=0.5)
    ldap_obj.simple_bind_s(current_app.config['YUSERS_LDAP_ADMIN_DN'], current_app.config['YUSERS_LDAP_ADMIN_PASSWORD'])
    return ldap_obj


def search(filter: str, attrs: list[str]) -> list[tuple[str, dict]]:
    """Search LDAP."""
    ldap_obj = get_ldap_object()
    return cast(list[tuple[str, dict]], ldap_obj.search_s(USERS_BASE, SCOPE_SUBTREE, filter, attrs))


def set_password(uid: str, password: str) -> None:
    """Set user's password to LDAP."""
    ldap_obj = get_ldap_object()
    ldap_obj.passwd_s(uid, None, password)
