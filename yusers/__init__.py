"""Yusers - Yunohost application for user management."""
import os
from logging.config import dictConfig
from typing import Any, Optional, cast

import jinja2
from flask import Flask, redirect, url_for
from flask_babel import Babel  # type: ignore[import-untyped]
from flask_mail import Mail
from flask_wtf.csrf import CSRFProtect  # type: ignore[import-untyped]
from werkzeug.utils import cached_property
from werkzeug.wrappers.response import Response

__version__ = '1.0.0'


BABEL = Babel()
CSRF = CSRFProtect()
MAIL = Mail()


def _redirect_index() -> Response:
    return redirect(url_for('password_reset.password_reset'))


class InstanceTemplateFlask(Flask):
    """Flask object which searches templates in the instance directory."""

    @cached_property
    def jinja_loader(self) -> Optional[jinja2.BaseLoader]:
        """The Jinja loader for this package bound object."""
        return jinja2.ChoiceLoader([jinja2.FileSystemLoader(self.config['YUSERS_CUSTOM_TEMPLATES_DIR']),
                                    cast(jinja2.BaseLoader, super().jinja_loader)])


def create_app(**config: Any) -> InstanceTemplateFlask:
    """Create and configure application."""
    app = InstanceTemplateFlask(__name__, instance_relative_config=True)
    app.config['YUSERS_CUSTOM_TEMPLATES_DIR'] = os.path.join(app.instance_path, 'templates')
    app.config['YUSERS_LDAP_URI'] = os.environ.get('YUSERS_LDAP_URI', 'ldap://localhost:389/')
    app.config['YUSERS_LDAP_ADMIN_DN'] = os.environ.get('YUSERS_LDAP_ADMIN_DN',
                                                        'uid=yusers,ou=users,dc=yunohost,dc=org')
    app.config['YUSERS_LDAP_ADMIN_PASSWORD'] = os.environ.get('YUSERS_LDAP_ADMIN_PASSWORD')
    app.config['YUSERS_LOGGING'] = None
    app.config['YUSERS_PASSWORD_RESET_TIMEOUT'] = os.environ.get('YUSERS_PASSWORD_RESET_TIMEOUT', 3)
    app.config['BABEL_TRANSLATION_DIRECTORIES'] = 'locale'
    app.config.from_envvar('YUSERS_SETTINGS', silent=True)
    app.config.from_mapping(**config)

    # Set up logging
    if app.config['YUSERS_LOGGING'] is not None:
        dictConfig(app.config['YUSERS_LOGGING'])

    # Set up translations.
    BABEL.init_app(app)
    # Set up CSRF protection.
    CSRF.init_app(app)
    # Set up mail
    MAIL.init_app(app)

    # Ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    from .password_reset import PASSWORD_RESET_BLUEPRINT
    app.register_blueprint(PASSWORD_RESET_BLUEPRINT)

    # Redirect index to password reset.
    app.add_url_rule('/', 'index', _redirect_index)

    return app
