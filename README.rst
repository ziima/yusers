======
yusers
======

Yunohost application for user management.

Features
========

* Password reset managed solely by users.

Installation
============

Yusers can only be installed from command line::

  cd /tmp
  curl -L -O "https://gitlab.com/ziima/yusers/-/archive/v1.0.0-ynh2/yusers-v1.0.0-ynh2.tar.gz"
  yunohost app install "/tmp/yusers-v1.0.0-ynh2.tar.gz"

Make sure ``admin`` account used by yusers have permission to read and modify ``userPassword``.
Yunohost LDAP configuration is in ``/usr/share/yunohost/conf/slapd/config.ldif``.

Upgrade
=======

Upgrade can only be performed from command line::

  cd /tmp
  curl -L -O "https://gitlab.com/ziima/yusers/-/archive/v1.0.0-ynh2/yusers-v1.0.0-ynh2.tar.gz"
  yunohost app upgrade yusers --file "/tmp/yusers-v1.0.0-ynh2.tar.gz"

Configuration
=============

The application is configured through the Flask standard settings file, defined by ``YUSERS_SETTINGS`` environment variable.
When installed to Yunohost, the file is located at ``/var/www/yusers/settings.py``.

Links to settings of used libraries:

* Flask-Babel: https://flask-babel.tkte.ch/#configuration
* Flask-Mail: https://pythonhosted.org/Flask-Mail/#configuring-flask-mail
* Flask-WTF: https://flask-wtf.readthedocs.io/en/stable/config.html
* Flask: https://flask.palletsprojects.com/en/1.1.x/config/#builtin-configuration-values

Yusers specific settings:

``YUSERS_CUSTOM_TEMPLATES_DIR``
-------------------------------
Path to a directory with custom templates.
Default is ``templates`` subdirectory of an instance directory.

``YUSERS_LDAP_ADMIN_DN``
------------------------
Distinguished name of the Yunohost administrator account.
Default is ``uid=yusers,ou=users,dc=yunohost,dc=org``, which corresponds to the legacy admin account in Yunohost 11.1.

``YUSERS_LDAP_ADMIN_PASSWORD``
------------------------------
Password of the Yunohost administrator account.
Default is ``None``, but the password needs to be provided for yusers to work.

``YUSERS_LDAP_URI``
-------------------
URI of the LDAP server.
Default is ``ldap://localhost:389/``.

``YUSERS_LOGGING``
------------------
Configuration of logging as expected by `dictConfig <https://docs.python.org/3/library/logging.config.html#logging.config.dictConfig>`_
Default is ``None``, i.e. use Flask defaults.

``YUSERS_PASSWORD_RESET_TIMEOUT``
---------------------------------
Number of days for which a password reset token is valid.
Default is ``3``.

Customization
=============

Custom templates can be provided by inserting them into ``templates`` subdirectory of instance path.
That is ``/var/www/yusers/instance/templates`` on Yunohost by default.
It can be changed by the ``YUSERS_CUSTOM_TEMPLATES_DIR`` setting.

Custom templates may be put in ``override`` subdirectory, to inherit from the original templates.

Development
===========

Flask application itself can be run locally from repository root::

  PYTHONPATH=. FLASK_APP=yusers FLASK_DEBUG=true YUSERS_SETTINGS=my_settings.py flask run

Whole Yunohost can be run using a docker::

  docker run -d -h yunohost.local --name=yunohost --privileged -p 80:80 -p 443:443 -v /sys/fs/cgroup:/sys/fs/cgroup:ro domainelibre/yunohost && \
  docker exec -it yunohost yunohost tools postinstall -d yunohost.local -p HesloHeslo && \
  docker exec -it yunohost yunohost app install https://gitlab.com/ziima/yusers --force --args "domain=yunohost.local&path=/password_reset&is_public=yes&admin_password=HesloHeslo&default_sender=root@localhost&language=en"

Upgrade to main
===============

To upgrade app to a current main branch::

  cd /tmp
  git clone https://gitlab.com/ziima/yusers.git
  yunohost app upgrade yusers --force --file /tmp/yusers

Release
=======

YunoHost doesn't quite support having single repository for the application itself and YunoHost intergration.
To bypass that, we tag application first and then update manifest and tag the YuhoHost integration.

References
==========

YunoHost LDAP schema: https://moulinette.readthedocs.io/en/latest/ldap.html
