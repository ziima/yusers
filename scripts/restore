#!/bin/bash
set -eu

#=================================================
# IMPORT GENERIC HELPERS
#=================================================

source /usr/share/yunohost/helpers

#=================================================
# RESTORE THE APP MAIN DIR
#=================================================
ynh_script_progression "Restoring the app main directory..."

ynh_restore_file "$install_dir"

### $install_dir will automatically be initialized with some decent
### permissions by default... however, you may need to recursively reapply
### ownership to all files such as after the ynh_setup_source step
chown -R "$app:www-data" "$install_dir"

#=================================================
# RESTORE SYSTEM CONFIGURATIONS
#=================================================
ynh_script_progression "Restoring system configurations related to $app..."

ynh_restore_file "/etc/nginx/conf.d/$domain.d/$app.conf"

ynh_restore_file "/etc/systemd/system/$app.service"
systemctl enable "$app.service" --quiet

yunohost service add "$app" --description="Yunohost user management application." --log="/var/log/$app/$app.log"

ynh_restore_file "/etc/logrotate.d/$app"

# Add user to a www-data group. uWSGI requires that to change group of the uWSGI socket.
adduser yusers www-data

#=================================================
# RESTORE VARIOUS FILES
#=================================================

ynh_restore_file "/etc/uwsgi/uwsgi_$app.py"
ynh_restore_file "/etc/uwsgi/apps-available/$app.ini"
ynh_restore_file "/etc/tmpfiles.d/$app.conf"

#=================================================
# RELOAD NGINX AND PHP-FPM OR THE APP SERVICE
#=================================================
ynh_script_progression "Reloading NGINX web server and $app's service..."

ynh_systemd_action --service_name="$app" --action="start"

ynh_systemd_action --service_name=nginx --action=reload

#=================================================
# END OF SCRIPT
#=================================================

ynh_script_progression "Restoration completed for $app"
